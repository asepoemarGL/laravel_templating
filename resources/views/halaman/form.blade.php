<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>

    <h2>Sign Up Form</h2>

    <form action="/sukses" method="post">
        @csrf
        <label>First name:</label> <br>
        <input type="text" name="namadpn"> <br><br>
        <label>Last name:</label> <br>
        <input type="text" name="namablk"> <br><br>
        <label>Gender:</label> <br>
        <input type="radio" name="gender" value="male">Male <br>
        <input type="radio" name="gender" value="female">Female <br>
        <input type="radio" name="gender" value="other">Other <br><br>

        <label>Nationality:</label> <br>
        <select name="nationality" >
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select><br><br>

        <label>language Spoken:</label> <br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa">English <br>
        <input type="checkbox" name="bahasa">Other <br><br>

        <label>Bio:</label> <br>
        <textarea name="biodata" id="" cols="30" rows="10"></textarea><br>


        <input type="submit" value="Kirim">
</body>
</html>