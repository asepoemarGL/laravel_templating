<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }

    public function kirim(Request $request){
        // dd($request->all());
        $namadpn = $request['namadpn'];
        $namablk = $request['namablk'];
        
        return view('halaman.sukses',compact('namadpn','namablk'));
    }
}
